#include "taskimpl.h"
#if defined(__unix__) || defined(__APPLE__)
#include <sys/poll.h>
#include <sys/select.h>
#elif defined(_WIN32)
#pragma comment(lib, "Ws2_32.lib")
#include <io.h>
#include <WinSock2.h>
#define inline
#define poll WSAPoll
#define read _read
#define write _write
typedef unsigned long nfds_t;
#define errno WSAGetLastError()
#define EINPROGRESS WSAEWOULDBLOCK
#define write(fd, buf, len) send(fd, buf, len, 0)
#define read(fd, buf, len) recv(fd, buf, len, 0)
#endif
#include <fcntl.h>

enum
{
	MAXFD = 1024
};

static struct pollfd pollfd[MAXFD];
static Task *polltask[MAXFD];
static int npollfd;
static int startedfdtask;
static Tasklist sleeping;
static int sleepingcounted;
static uvlong nsec(void);

void
fdtask(void *v)
{
	(void)v;

	int i, ms;
	Task *t;
	uvlong now;

	tasksystem();
	taskname("fdtask");
	for(;;){
		/* let everyone else run */
		while(taskyield() > 0)
			;

		/* we're the only one runnable - poll for i/o */
#if defined(__unix__) || defined(__APPLE__)
		errno = 0;
#endif
		taskstate("poll");
		if((t=sleeping.head) == nil && (! anyready()))
			ms = -1;
		else{
			/* sleep at most 5s */
			now = nsec();
			if(now >= t->alarmtime)
				ms = 0;
			else if(now+5*1000*1000*1000LL >= t->alarmtime)
				ms = (t->alarmtime - now)/1000000;
			else
				ms = 5000;
		}
#if defined(_WIN32)
		/* using poll for sleeping does not work on win32 */
		if (npollfd == 0) {
			Sleep(ms);
		} else
#endif
		if(poll(pollfd, npollfd, ms) < 0){
			if(errno == EINTR)
			{
				/* wake up all tasks in poll state and bail out */
				for(i=0; i<npollfd; i++){
					while(i < npollfd){
						if (polltask[i]->alarmtime)
						{
							deltask(&sleeping, polltask[i]);
							if(!polltask[i]->system && --sleepingcounted == 0)
								taskcount--;
						}

						taskready(polltask[i]);
						--npollfd;
						pollfd[i] = pollfd[npollfd];
						polltask[i] = polltask[npollfd];
					}
				}
				startedfdtask = 0;
				taskexit(0);
			}

#if defined(__unix) || defined(__APPLE__)
			fprint(2, "poll: %s\n", strerror(errno));
#elif defined(_WIN32)
			fprint(2, "poll: %d\n", WSAGetLastError());
#endif
			taskexitall(0);
		}

		/* wake up the guys who deserve it */
		now = nsec();
		for(i=0; i<npollfd; i++){
			while(i < npollfd && (pollfd[i].revents || (polltask[i]->alarmtime && (now >= polltask[i]->alarmtime)))){
				if (polltask[i]->alarmtime)
				{
					deltask(&sleeping, polltask[i]);
					if(!polltask[i]->system && --sleepingcounted == 0)
						taskcount--;
				}

				taskready(polltask[i]);
				--npollfd;
				pollfd[i] = pollfd[npollfd];
				polltask[i] = polltask[npollfd];
			}
		}

		while((t=sleeping.head) && now >= t->alarmtime){
			deltask(&sleeping, t);
			if(!t->system && --sleepingcounted == 0)
				taskcount--;
			taskready(t);
		}
	}
}

static inline uvlong
tasktimeout(uint ms)
{
	uvlong when, now;
	Task *t;

	if(!startedfdtask){
		startedfdtask = 1;
		taskcreate(fdtask, 0, 32768);
	}

	now = nsec();
	when = now+(uvlong)ms*1000000;
	for(t=sleeping.head; t!=nil && t->alarmtime < when; t=t->next)
		;

	if(t){
		taskrunning->prev = t->prev;
		taskrunning->next = t;
	}else{
		taskrunning->prev = sleeping.tail;
		taskrunning->next = nil;
	}

	t = taskrunning;
	t->alarmtime = when;
	if(t->prev)
		t->prev->next = t;
	else
		sleeping.head = t;
	if(t->next)
		t->next->prev = t;
	else
		sleeping.tail = t;

	if(!t->system && sleepingcounted++ == 0)
		taskcount++;

	return now;
}

uint
taskdelay(uint ms)
{
	uvlong now;

	now = tasktimeout(ms);
	taskswitch();

	return (nsec() - now)/1000000;
}

void
_fdwait(int fd, int rw, uint ms)
{
	int bits;

	if(!startedfdtask){
		startedfdtask = 1;
		taskcreate(fdtask, 0, 32768);
	}

	if(npollfd >= MAXFD){
		fprint(2, "too many poll file descriptors\n");
		abort();
	}

	taskstate("fdwait for %s", rw=='r' ? "read" : rw=='w' ? "write" : "error");
	bits = 0;
	switch(rw){
	case 'r':
		bits |= POLLIN;
		break;
	case 'w':
		bits |= POLLOUT;
		break;
	}

	taskrunning->alarmtime = 0;
	polltask[npollfd] = taskrunning;
	pollfd[npollfd].fd = fd;
	pollfd[npollfd].events = bits;
	pollfd[npollfd].revents = 0;
	npollfd++;
	if (ms)
		tasktimeout(ms);
}

void
fdwait(int fd, int rw, uint ms)
{
	_fdwait(fd, rw, ms);
	taskswitch();
}

int
fdpoll(struct pollfd fds[], nfds_t nfds, int timeout)
{
	nfds_t n;
	int i;
	int bits;

	if (timeout == 0)
		return poll(fds, nfds, timeout);
	else if (timeout == -1)
		timeout = 0;

	for(n=0; n<nfds; n++){
		bits = 0;

		if (fds[n].events & POLLIN)
			bits = 'r';
		else if (fds[n].events & POLLOUT)
			bits = 'w';

		_fdwait(fds[n].fd, bits, timeout);
	}

	taskswitch();

	/* clean up after multiple _fdwait calls */
	for(i=0; i<npollfd; i++){
		if(polltask[i]->ready){
			--npollfd;
			pollfd[i] = pollfd[npollfd];
			polltask[i] = polltask[npollfd];
		}
	}

	/* hack to get poll output */
	return poll(fds, nfds, 0);
}

int
fdselect(int nfds, fd_set *readfds, fd_set *writefds, fd_set *exceptfds, struct timeval *timeout)
{
	struct timeval dummy = {0, 0};
	int i;
	int ms = 0;

	if(timeout){
		ms = timeout->tv_sec * 1000 + timeout->tv_usec / 1000;
		if(!ms)
			return select(nfds, readfds, writefds, exceptfds, &dummy);
	}

	for(i=0; i<nfds; i++){
		int bits = 0;

		if (readfds && FD_ISSET(i, readfds))
			bits = 'r';
		else if (writefds && FD_ISSET(i, writefds))
			bits = 'w';
		else if (exceptfds && FD_ISSET(i, exceptfds))
			bits = 'e';

		if (bits)
			_fdwait(i, bits, ms);
	}

	taskswitch();

	/* clean up after multiple _fdwait calls */
	for(i=0; i<npollfd; i++){
		if(polltask[i]->ready){
			--npollfd;
			pollfd[i] = pollfd[npollfd];
			polltask[i] = polltask[npollfd];
		}
	}

	/* hack to get select output */
	return select(nfds, readfds, writefds, exceptfds, &dummy);
}

/* Like fdread but always calls fdwait before reading. */
int
fdread1(int fd, void *buf, int n)
{
	int m;

	do
		fdwait(fd, 'r', 0);
	while((m = read(fd, buf, n)) < 0 && errno == EAGAIN);
	return m;
}

int
fdread(int fd, void *buf, int n)
{
	int m;
	
	while((m=read(fd, buf, n)) < 0 && errno == EAGAIN)
		fdwait(fd, 'r', 0);
	return m;
}

int
fdwrite(int fd, void *buf, int n)
{
	int m, tot;
	
	for(tot=0; tot<n; tot+=m){
		while((m=write(fd, (char*)buf+tot, n-tot)) < 0 && errno == EAGAIN)
			fdwait(fd, 'w', 0);
		if(m < 0)
			return m;
		if(m == 0)
			break;
	}
	return tot;
}

int
fdnoblock(int fd)
{
#if defined(__unix) || defined(__APPLE__)
	return fcntl(fd, F_SETFL, fcntl(fd, F_GETFL)|O_NONBLOCK);
#elif defined(_WIN32)
	int mode = 1;
	return ioctlsocket(fd, FIONBIO, &mode);
#endif
}

#if defined(_WIN32)
#define CLOCK_MONOTONIC 0

struct timespec {
	time_t tv_sec;
	long tv_nsec;
};

int
clock_gettime(int clk_id, struct timespec *ts)
{
	LARGE_INTEGER           t;
	double                  microseconds;
	static LARGE_INTEGER    offset;
	static double           frequencyToMicroseconds;
	static int              initialized = 0;

	if (!initialized) {
		LARGE_INTEGER performanceFrequency;
		if (QueryPerformanceFrequency(&performanceFrequency)) {
			initialized = 1;
			QueryPerformanceCounter(&offset);
			frequencyToMicroseconds = (double)performanceFrequency.QuadPart / 1000000.;
		}
		else {
			return -1;
		}
	}

	QueryPerformanceCounter(&t);

	t.QuadPart -= offset.QuadPart;
	microseconds = (double)t.QuadPart / frequencyToMicroseconds;
	t.QuadPart = microseconds;
	ts->tv_sec = t.QuadPart / 1000000;
	ts->tv_nsec = (t.QuadPart % 1000000) * 1000;
	return (0);
}
#endif

static uvlong
nsec(void)
{
	struct timespec ts;

	if(clock_gettime(CLOCK_MONOTONIC, &ts) < 0)
		return -1;
	return (uvlong)ts.tv_sec*1000*1000*1000 + ts.tv_nsec;
}

