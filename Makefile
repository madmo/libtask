LIB=libtask.a
SONAME=libtask.so.0
SHLIB=$(SONAME).0.1
TCPLIBS=
PREFIX?=/usr/local

ASM=asm.o
OFILES=\
	$(ASM)\
	channel.o\
	context.o\
	fd.o\
	net.o\
	print.o\
	qlock.o\
	rendez.o\
	task.o\

all: $(LIB) $(SHLIB) primes tcpproxy testdelay testwait

$(OFILES): taskimpl.h task.h 386-ucontext.h power-ucontext.h

AS=$(CC) -c
CFLAGS += -W -Wall -Wextra -c -I. -ggdb -fPIC

%.o: %.S
	$(AS) $*.S

%.o: %.c
	$(CC) $(CFLAGS) $*.c

$(LIB): $(OFILES)
	$(AR) rvc $(LIB) $(OFILES)

$(SHLIB): $(OFILES)
	$(CC) -shared -fPIC -o $@ -Wl,-soname,$(SONAME) $(OFILES)

primes: primes.o $(LIB)
	$(CC) -o primes primes.o $(LIB)

tcpproxy: tcpproxy.o $(LIB)
	$(CC) -o tcpproxy tcpproxy.o $(LIB) $(TCPLIBS)

httpload: httpload.o $(LIB)
	$(CC) -o httpload httpload.o $(LIB)

testdelay: testdelay.o $(LIB)
	$(CC) -o testdelay testdelay.o $(LIB)

testdelay1: testdelay1.o $(LIB)
	$(CC) -o testdelay1 testdelay1.o $(LIB)

testwait: testwait.o $(LIB)
	$(CC) -o testwait testwait.o $(LIB)

clean:
	rm -f *.o primes tcpproxy testdelay testdelay1 testwait httpload $(LIB) $(SHLIB)

install: $(LIB) $(SHLIB)
	install -m 755 -d $(DESTDIR)/$(PREFIX)/lib
	install -m 755 -d $(DESTDIR)/$(PREFIX)/include
	install -m 644 $(LIB) $(DESTDIR)/$(PREFIX)/lib
	install -m 755 $(SHLIB) $(DESTDIR)/$(PREFIX)/lib
	install -m 644 task.h $(DESTDIR)/$(PREFIX)/include
	ln -sf $(SHLIB) $(DESTDIR)/$(PREFIX)/lib/libtask.so
	ln -sf $(SHLIB) $(DESTDIR)/$(PREFIX)/lib/$(SONAME)
