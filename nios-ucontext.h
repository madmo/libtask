#ifndef _NIOS_UCONTEXT_H
#define _NIOS_UCONTEXT_H

#define NGREG   32
#define NIOS_MAXREGWINDOW	31

typedef int greg_t;
typedef greg_t  gregset_t[NGREG];

struct  rwindow
{
	greg_t rw_local[8];			/* locals */
	greg_t rw_in[8];			/* ins */
};

#define rw_fp   rw_in[6]		/* frame pointer */
#define rw_rtn  rw_in[7]		/* return address */

typedef struct gwindows
{
	int            wbcnt;
	int           *spbuf[NIOS_MAXREGWINDOW];
	struct rwindow wbuf[NIOS_MAXREGWINDOW];
} gwindows_t;

typedef struct
{
	gregset_t   gregs;		/* general register set */
	gwindows_t  *gwins;		/* POSSIBLE pointer to register windows */
} mcontext_t;

typedef struct sigaltstack
{
	void *ss_sp;
	size_t ss_size;
	int ss_flags;
} stack_t;

/* Userlevel context.  */
typedef struct ucontext
{
	unsigned long   uc_flags;
	mcontext_t      uc_mcontext;
	stack_t         uc_stack;
} ucontext_t;

extern void makecontext(ucontext_t *uc, void (*fn)(void), int argc, ...);
extern int swapcontext(ucontext_t*, const ucontext_t*);

#endif
